package com.example.noteviewer2;

import com.example.pages.Note;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditActivity extends Activity implements OnClickListener {
	private Button viewButton; // ������ �������� � ��������� �������
	private EditText editText; // ���� �������������� ������
	private Note currentNote;	// �������� ������ �� ������� �������

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);

		init();
	}

	private void init() {
		viewButton = (Button) findViewById(R.id.viewButton);
		editText = (EditText) findViewById(R.id.editText);

		viewButton.setOnClickListener(this);
		currentNote = (Note)Global.getInstance().getCurrentPage();

		editText.setText(currentNote.getUserText());
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.viewButton: // ��������� HTML ��������
			openWebView();
			break;
		}
	}

	/**
	 * ��������� � ��������� ����������� � ������� HTML
	 */
	private void openWebView() {
		currentNote.setUserText(editText.getText().toString());
		currentNote.saveNoteToFile();

		Intent intent = new Intent(this, WebViewActivity.class);
		startActivity(intent);
	}

}
