package com.example.noteviewer2;


import com.example.pages.Note;
import com.example.pages.Page;
import com.example.pages.WebPage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class WebViewActivity extends Activity implements OnClickListener {
	
	private WebView htmlBrowser; // ������� ��� ��������� �������� HTML

	private Button editButton;
	private Button backButton;
	private Button homeButton;
	private Button forwardButton;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);
		
		init();
	}
	
	private void init() {
		
		Global.getInstance().init(this);
		
		htmlBrowser = (WebView) findViewById(R.id.htmlBrowser);
		editButton = (Button) findViewById(R.id.editButton);
		backButton = (Button) findViewById(R.id.backButton);
		homeButton = (Button) findViewById(R.id.homeButton);
		forwardButton = (Button) findViewById(R.id.forwardButton);
		
		htmlBrowser.setWebViewClient(new HelloWebViewClient());
		editButton.setOnClickListener(this);
		backButton.setOnClickListener(this);
		homeButton.setOnClickListener(this);
		forwardButton.setOnClickListener(this);
		
		String curUrl = Global.getInstance().getCurrentPage().getUrl();
		
		htmlBrowser.loadUrl(curUrl);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.web_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.editButton:
			openEditView();
			break;
		case R.id.backButton:
			//goBack();
			break;
		case R.id.forwardButton:
			//goForward();
			break;
		case R.id.homeButton:
			goHome();
			break;
		default:
			break;
		}
	}
	
	/**
	 * ������� ���� �������������� �������
	 */
	private void openEditView() {
		Intent intent = new Intent(this, EditActivity.class);
		startActivity(intent);
	}
	
	private void goHome() {
		Global.getInstance().goToHomePage();
		htmlBrowser.loadUrl(Global.getInstance().getCurrentPage().getUrl());
	}
	
	private class HelloWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			Global.getInstance().setCurrentPage(url);
			
			if (Global.getInstance().getCurrentPage() instanceof Note) {
				editButton.setEnabled(true);
			} else if (Global.getInstance().getCurrentPage() instanceof WebPage) {
				editButton.setEnabled(false);
			}
			
			return true;
		}
	}
}
