package com.example.noteviewer2;

import com.example.pages.Note;
import com.example.pages.Page;
import com.example.pages.WebPage;

import android.content.Context;

/**
 * �����-��������, ���������� ���������� ������
 * 
 * @author ���������
 *
 */
public class Global {
	
	private static Global instance = new Global();
	
	private Context appContext; // �������� ����������
	private boolean isInit; 	// ���� �������������
	
	private UserTextParser userTextParser;	// ������ ��� �������� wiki-�������� � html ������
	private StyleParser styleParser;		// ������ ��� ���������� ������ � html ����
	private Utils utils;					// ����� ����������� �������

	private Page currentPage;				// ������ �� ������������ � ������ ������ ��������
	
	private final String HOMEPAGE = "homepage"; // ��� ��������� ��������

	public static Global getInstance() {
		return instance;
	}

	/**
	 * ������������� ����������� �������
	 * 
	 * @param context
	 */
	public void init(Context context) {
		if (!isInit) {
			appContext = context;
			isInit = true;
			userTextParser = new UserTextParser();
			styleParser = new StyleParser(context);
			utils = new Utils(context);
			
			String startUrl = getHomePageUrl();
			setCurrentPage(startUrl);
		}
	}
	
	public String getHomePageUrl() {
		return utils.getUrlFromName(HOMEPAGE);
	}
	
	public Context getAppContext() {
		return appContext;
	}
	
	public UserTextParser getUserTextParser() {
		return userTextParser;
	}
	
	public StyleParser getStyleParser() {
		return styleParser;
	}
	
	public Utils getUtils() {
		return utils;
	}
	
	public Page getCurrentPage() {
		return currentPage;
	}
	
	/**
	 * ����������� ���������� �� �������� � �������� url
	 */
	public void setCurrentPage(String url) {
		boolean isInner = Global.getInstance().getUtils().isInnerPage(url);
		if (isInner) {	// ���� �������� �������� �� ����������
			currentPage = new Note(url);
		} else {	// ���� �������� �� ���������
			currentPage = new WebPage(url);
		}
	}
	
	public void goToHomePage() {
		setCurrentPage(getHomePageUrl());
	}
}
