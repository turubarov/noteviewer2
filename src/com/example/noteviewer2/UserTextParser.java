package com.example.noteviewer2;

/**
 * ����� ����������� �����, �������� �������������, � ������ html
 * 
 * @author ���������
 * 
 */
public class UserTextParser {

	// private static UserTextParser instance = new UserTextParser();

	public enum Lexems {
		USER_TEXT, BOLD, ITALIC, STRIKE, EMPH, HEADER, NEXT_STR, END_TEXT, NUMERED_LIST, MARKED_LIST, QUOTE, LINE, FRAME_OPEN, FRAME_CLOSE, TABLE_DELIMETER, NOT_MARKED_OPEN, NOT_MARKED_CLOSE, HTML_CODE, IMAGE, LINK, ANOTHER_PAGE
	}; // ������������ ��������� ����� ������ ����������������� ������

	private String userText; // ���������������� �����
	private int userTextLenght; // ����� ����������������� ������

	private String currentLexemString; // ��������� ����������� ������� �������
	private String currentLinkString; // ����������� ���� ��� ������ ������
	private String currentLinkNameString;
	private String pageName; // ��� ��������, ����������� � �������
	private int currentPosition; // ������� ������� ���������
	private Lexems currentLexem; // ������� ��� �������
	private Lexems prevLexem; // ��� ���������� �������

	private boolean isBold; // ����������, ������������ ������� ���
	private boolean isItalic;
	private boolean isStrike;
	private boolean isEmph;
	private boolean isQuote;
	private boolean isTable;

	private int currentNumeredListDepth; // ������� ������� ������������� ������
	private int prevNumeredListDepth; // ������� ������������� ������ ��
										// ���������� ������

	private int currentMarkedListDepth; // ������� ������� ������������� ������
	private int prevMarkedListDepth; // ������� ������������� ������ ��
										// ���������� ������e

	private int levelOfHeader; // ������� �������� ���������

	private char curChar; // ������ �� ������� ������� ��������� currentPosition
	private char prevChar;
	private char nextChar;

	// private boolean isLinkInString; // ���� ��������������� � true, ���� �
	// ������� ������ ��� ������
	// private boolean isNamedLinkInString; // ���� ��������������� � true, ����
	// � ������� ������ ��� ����������� ������

	// private UserTextScaner scaner; // ������ ������ �� �����������������
	// ������

	public UserTextParser() {
		// scaner = new UserTextScaner();
	}

	/*
	 * public static UserTextParser getInstance() { return instance; }
	 */

	// �������������� ����������������� ������ � HTML
	public String parse(String userString) {
		// scaner.setScanningText(userString);
		currentPosition = 0;
		userText = userString;
		userTextLenght = userText.length();
		userText += "\n";
		isBold = isItalic = isStrike = isEmph = isTable = false;
		currentNumeredListDepth = prevNumeredListDepth = 0;
		currentMarkedListDepth = prevMarkedListDepth = 0;
		levelOfHeader = 0;

		prevChar = '\n';
		curChar = userText.charAt(currentPosition);
		if (userTextLenght > 0)
			nextChar = userText.charAt(currentPosition + 1);
		else
			nextChar = '\n';

		String result = "";

		Lexems nextLexem;
		int storedPosition;

		while ((currentLexem = Scan()) != Lexems.END_TEXT) { // ��������� HTML
																// �� ���������
																// ������
			if (prevLexem == Lexems.NEXT_STR) { // ��������� ������, ����
												// �����
				if (currentLexem != Lexems.NUMERED_LIST && prevNumeredListDepth > 0) {
					for (int i = 0; i < prevNumeredListDepth; i++)
						result += "</ol>";
					currentNumeredListDepth = 0;
				}
				if (currentLexem != Lexems.MARKED_LIST && prevMarkedListDepth > 0) {
					for (int i = 0; i < prevMarkedListDepth; i++)
						result += "</ul>";
					currentMarkedListDepth = 0;
				}
			}
			switch (currentLexem) {
			case USER_TEXT:
				result += currentLexemString;
				break;
			case BOLD:
				isBold = !isBold;
				if (isBold)
					result += "<strong>";
				else
					result += "</strong>";
				break;
			case ITALIC:
				isItalic = !isItalic;
				if (isItalic)
					result += "<em>";
				else
					result += "</em>";
				break;
			case STRIKE:
				isStrike = !isStrike;
				if (isStrike)
					result += "<strike>";
				else
					result += "</strike>";
				break;
			case EMPH:
				isEmph = !isEmph;
				if (isEmph)
					result += "<u>";
				else
					result += "</u>";
				break;
			case HEADER:
				result += ("<h" + Integer.toString(levelOfHeader) + ">");
				break;
			case QUOTE:
				isQuote = true;
				result += "<blockquote><p>";
				break;
			case LINE:
				result += "<hr>";
				break;
			case NEXT_STR:
				if (levelOfHeader > 0)
					result += ("</h" + Integer.toString(levelOfHeader) + ">");
				else if (currentNumeredListDepth > 0 || currentMarkedListDepth > 0)
					result += "</li>";
				else if (isQuote)
					result += "</p></blockquote>";
				else if (isTable) {
					storedPosition = currentPosition; // ��������� ���������
														// �������
					nextLexem = Scan();
					setPosition(storedPosition);
					if (nextLexem != Lexems.TABLE_DELIMETER) { // ����� �������
																// �������
						isTable = false;
						result += "</td></tr></table>";
					} else { // ����� ������� ������ �������
						result += "</td></tr>";
					}
				} else
					result += "<br>";
				levelOfHeader = 0;
				prevNumeredListDepth = currentNumeredListDepth;
				prevMarkedListDepth = currentMarkedListDepth;
				break;
			case END_TEXT:
				break;
			case NUMERED_LIST: // ������������ ������������� ������
				if (prevNumeredListDepth < currentNumeredListDepth) { // ��������
																		// �����������
																		// ������
					for (int i = prevNumeredListDepth; i < currentNumeredListDepth; i++)
						result += "<ol>";
				}
				if (prevNumeredListDepth > currentNumeredListDepth) { // ��������
																		// �����������
																		// ������
					for (int i = currentNumeredListDepth; i < prevNumeredListDepth; i++)
						result += "</ol>";
				}
				result += "<li>";
				break;
			case MARKED_LIST: // ������������ �������������� ������
				if (prevMarkedListDepth < currentMarkedListDepth) { // ��������
																	// �����������
																	// ������
					for (int i = prevMarkedListDepth; i < currentMarkedListDepth; i++)
						result += "<ul>";
				}
				if (prevMarkedListDepth > currentMarkedListDepth) { // ��������
																	// �����������
																	// ������
					for (int i = currentMarkedListDepth; i < prevMarkedListDepth; i++)
						result += "</ul>";
				}
				result += "<li>";
				break;
			case FRAME_OPEN: // ��������� ����� � �����
				result += "<p><pre>";
				break;
			case FRAME_CLOSE: // ��������� ����� � �����
				result += "</pre></p>";
				break;
			case TABLE_DELIMETER: // ������������ �������
				if (!isTable) { // ����� ������� �������
					isTable = true;
					result += "<table border=1><tr><td>";
				} else {
					if (prevLexem == Lexems.NEXT_STR)
						result += "<tr><td>";
					else
						result += "</td><td>";
				}
				break;
			case NOT_MARKED_OPEN:
				result += "<p>";
				break;
			case NOT_MARKED_CLOSE:
				result += "</p>";
				break;
			case HTML_CODE:
				result += currentLexemString;
				break;
			case IMAGE:
				result += ("<img src=" + "\"" + currentLexemString + "\"" + "/>");
				break;
			case LINK: // ������ �� ��������
				if (currentLinkString.indexOf(".") == -1) {
					if (currentLinkNameString.length() == 0)
						currentLinkNameString = currentLinkString;
					currentLinkString = currentLinkString + ".html";
				} else {
					if (currentLinkNameString.length() == 0)
						currentLinkNameString = currentLinkString;
					currentLinkString = "http://" + currentLinkString;
				}
				if (currentLinkNameString.length() > 0)
					result += ("<a href=" + "\"" + currentLinkString + "\"" + ">" + currentLinkNameString + "</a>");
				else
					result += ("<a href=" + "\"" + currentLinkString + "\"" + ">" + currentLinkString + "</a>");
				break;
			case ANOTHER_PAGE:
				/*result += parse(FileManager.getInstance().loadFromFile(pageName + ".txt"));
				result += ("<p align=\"right\"><a href = " + pageName + ".html" + ">" + "view page" + "</a></p>");
				break;*/
			}

			prevLexem = currentLexem; // ���������� ���������� �������
		}

		return result;
	}

	/**
	 * ��������� ��������� ������� �� ����������������� ������
	 * 
	 * @return ��� ��������� �������
	 */
	private Lexems Scan() {
		Lexems result = Lexems.USER_TEXT;
		currentLexemString = "";
		if (currentPosition >= userTextLenght)
			return Lexems.END_TEXT;

		switch (curChar) {
		/*
		 * case ' ': if (prevChar == '\n' && nextChar == '-') { result =
		 * Lexems.STRIKE; readByAmount(2); break; }
		 */
		// ��������� �������������� �������
		case '*':
			if (nextChar != '*') { // ������ �����
				result = Lexems.BOLD;
				readByAmount(1);
				break;
			} else { // ���� ������ *
				result = Lexems.USER_TEXT;
				readByAmount(2);
				currentLexemString = "*";
			}
		case '=': // ������
			if (nextChar != '=') {
				result = Lexems.ITALIC;
				readByAmount(1);
			} else { // ���� ������ =
				result = Lexems.USER_TEXT;
				readByAmount(2);
				currentLexemString = "=";
			}
			break;
		case '_':
			char nextNextChar;
			if (currentPosition < userText.length() - 2)
				nextNextChar = userText.charAt(currentPosition + 2);
			else
				nextNextChar = '\n';
			if (nextChar == '_' && nextNextChar == '_') { // �����
				result = Lexems.LINE;
				readByAmount(3);
			} else {
				if (nextChar != '_') { // ������������ �����
					result = Lexems.EMPH;
					readByAmount(1);
				} else { // ������ _
					result = Lexems.USER_TEXT;
					readByAmount(2);
					currentLexemString = "_";
				}
			}
			break;
		case '-':
			if (prevChar != '\n') {
				// ������ '-' ������� ������ - ������������� �����
				if (nextChar != '-') {
					result = Lexems.STRIKE;
					readByAmount(1);
				} else {
					result = Lexems.USER_TEXT;
					readByAmount(2);
					currentLexemString = "-";
				}
			} else { // ������ '-' � ������ ������ - ������ ��������������
						// ������
				currentMarkedListDepth = readUntil('-');
				result = Lexems.MARKED_LIST;
			}

			break;
		case '>':
			if (prevChar == '\n') { // ������
				result = Lexems.QUOTE;
				readByAmount(1);
			}
			break;
		case '[':
			if (nextChar == '|') {
				result = Lexems.FRAME_OPEN;
				readByAmount(2);
			} else if (nextChar == '{') {
				result = Lexems.NOT_MARKED_OPEN;
				readByAmount(2);
			} else if (nextChar == '$') {
				result = Lexems.IMAGE;
				incrimentPosition(2);
				readWhile("$]");
				incrimentPosition(2);
			} else if (nextChar == '<') {
				result = Lexems.HTML_CODE;
				incrimentPosition(2);
				readWhile(">]");
				incrimentPosition(2);
			} else if (nextChar == '[') { // ������ � ������
				incrimentPosition(2);
				readWhile("]");
				currentLinkString = currentLexemString;
				incrimentPosition(2);
				readWhile("]");
				currentLinkNameString = currentLexemString;
				incrimentPosition(2);
				result = Lexems.LINK;
			} else if (nextChar == '^') {
				incrimentPosition(2);
				readWhile("]");
				pageName = currentLexemString;
				incrimentPosition(1);
				result = Lexems.ANOTHER_PAGE;
			} else { // ������ ������
				currentLinkString = "";
				currentLinkNameString = "";
				incrimentPosition(1);
				readWhile("]");
				currentLinkString = currentLexemString;
				result = Lexems.LINK;
				incrimentPosition(1);
			}
			break;
		case '}':
			if (nextChar == ']') {
				result = Lexems.NOT_MARKED_CLOSE;
				readByAmount(2);
			}
			break;
		case '|': // ������ �������� �����
			if (nextChar == ']') {
				result = Lexems.FRAME_CLOSE;
				readByAmount(2);
				break;
			} else { // ����������� ��������� �����
				if (prevChar == '\n' || isTable) {
					result = Lexems.TABLE_DELIMETER;
					readByAmount(1);
					break;
				}
			}

		case '\n':
			result = Lexems.NEXT_STR;
			readByAmount(1);
			break;
		case '+': // ������� ���������� ������ '+', ����� ���������� �������
					// ���������
			if (prevChar == '\n') {
				levelOfHeader = readUntil('+');
				result = Lexems.HEADER;

				// ���� ������� ������ 6 �������� '+'
				if (levelOfHeader > 6) {
					incrimentPosition(6 - levelOfHeader);
					levelOfHeader = 6;
				}
				break;
			}
		case '#': // ��������� ���� ������������� ������
			if (prevChar == '\n') {
				currentNumeredListDepth = readUntil('#');
				result = Lexems.NUMERED_LIST;
				break;
			}
		default: // ��������� ������� �����
			while (!isUserTextTerminate()) {
				currentLexemString += Character.toString(curChar);
				if (!incrimentPosition(1))
					break;
			}
			break;
		}
		return result;
	}

	/**
	 * ������� ��������� �� ������� ������
	 * 
	 * @param steps
	 *            ���������� ��������
	 * @return ���������� true, ���� �� ��������� ����� ������
	 */
	private boolean incrimentPosition(int steps) {
		currentPosition += steps;
		if (currentPosition > userTextLenght - 1) { // ��������� ��������� ��
													// ����� �� ������� ������
			return false;
		}

		if (steps == 1) {
			prevChar = curChar;
			curChar = nextChar;
		} else {
			prevChar = userText.charAt(currentPosition - 1);
			curChar = userText.charAt(currentPosition);
		}

		if (currentPosition < userTextLenght - 1)
			nextChar = userText.charAt(currentPosition + 1);
		else
			nextChar = '\n';
		return true;
	}

	/**
	 * ��������� ��������� �� ������� ������ �� ������� ������
	 * 
	 * @param position
	 *            ������� ���������
	 * @return true, ���� ��������� ����������. false, ���� ��������� ������ ��
	 *         ����������
	 */
	private boolean setPosition(int position) {
		if (position < 0 || position > userTextLenght - 1)
			return false;
		currentPosition = position;
		if (currentPosition > 0)
			prevChar = userText.charAt(currentPosition - 1);
		else
			prevChar = '\n';
		curChar = userText.charAt(currentPosition);
		if (currentPosition < userTextLenght - 1)
			nextChar = userText.charAt(currentPosition + 1);
		else
			nextChar = '\n';
		return true;
	}

	/**
	 * ����������, �������� �� ������� ������� ���������� ������
	 * 
	 * @return
	 */
	private boolean isUserTextTerminate() {
		if (curChar == '#' && prevChar == '\n')
			return true;
		if (curChar == '-')
			return true;
		if (curChar == '*' && nextChar != '*')
			return true;
		if (curChar == '=' && nextChar != '=')
			return true;
		if (curChar == '_' && nextChar != '_')
			return true;
		if (curChar == '_' && nextChar == '_' && getSymbolByPosition(currentPosition + 2) == '_')
			return true;
		if (curChar == '|' && (prevChar == '\n' || isTable))
			return true;
		if (curChar == '|' && nextChar == ']')
			return true;
		if (curChar == '>' && nextChar == ']')
			return true;
		if (curChar == '}' && nextChar == ']')
			return true;
		if (curChar == '>' && prevChar == '\n')
			return true;
		if (curChar == '>' && nextChar == ']')
			return true;
		if (curChar == '[')
			return true;
		if (curChar == '\n')
			return true;
		return false;
	}

	/**
	 * ��������� �� ������� ������ �������� ���������� �������� � ������������
	 * ��������� � ���������� ��������� �������� � ������� �������
	 * 
	 * @param amount
	 * @return
	 */
	private int readByAmount(int amount) {
		int r_amount = amount;
		if (amount > userTextLenght - currentPosition) {
			r_amount = userTextLenght - currentPosition;
		}
		currentLexemString = userText.substring(currentPosition, currentPosition + r_amount - 1);
		incrimentPosition(amount);
		return amount;
	}

	/**
	 * ��������� �� ������� ������ ����������� ������������������ ��������
	 * ��������
	 * 
	 * @param symbol
	 *            ������ ������������������
	 * @param maxAmount
	 *            ������������ ���������� ����������� ���������
	 * @return ���������� ��������� ��������
	 */
	private int readUntil(char symbol, int maxAmount) {
		int amount = 0;
		while (curChar == symbol && amount < maxAmount) {
			currentLexemString += Character.toString(curChar);
			incrimentPosition(1);
			amount++;
		}
		return amount;
	}

	private int readUntil(char symbol) {
		return readUntil(symbol, Integer.MAX_VALUE);
	}

	/**
	 * ��������� �� ������� ������ ������������������ �� �������� ���������
	 * 
	 * @param subStr
	 * @return ���������� ��������� ��������
	 */
	private int readWhile(String subStr) {
		int amount = 0;
		int indexOfEnd = userText.indexOf(subStr, currentPosition);
		if (indexOfEnd == -1) // ���� �� ��������� ������ ���������, ������ ��
								// �����
			indexOfEnd = userTextLenght - 1;
		currentLexemString = userText.substring(currentPosition, indexOfEnd);
		amount = indexOfEnd - currentPosition;
		incrimentPosition(amount);
		return amount;
	}

	private char getSymbolByPosition(int position) {
		if (position < 0 || position > userTextLenght - 1)
			return '\n';
		else
			return userText.charAt(position);
	}

	/*
	 * private boolean checkLinkOnString() { int endStrIndex =
	 * userText.indexOf('\n', currentPosition); int sqBrIndex =
	 * userText.indexOf(']', currentPosition); return (sqBrIndex < endStrIndex)
	 * && sqBrIndex != -1; }
	 * 
	 * private boolean checkNamedLinkOnString() { int endStrIndex =
	 * userText.indexOf('\n', currentPosition); int sqBrIndex =
	 * userText.indexOf(']', currentPosition); if (sqBrIndex < endStrIndex &&
	 * sqBrIndex != -1) { if (userText.charAt(sqBrIndex + 1) == '[') { sqBrIndex
	 * = userText.indexOf(']', sqBrIndex + 1); return (sqBrIndex < endStrIndex)
	 * && sqBrIndex != -1; } else { return false; } } else { return false; } }
	 */
}
