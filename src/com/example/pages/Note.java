package com.example.pages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.widget.Toast;

import com.example.noteviewer2.Global;
import com.example.noteviewer2.StyleParser;
import com.example.noteviewer2.UserTextParser;

/**
 * ����� �������, ���������� � ������ ����������
 * 
 * @author ���������
 *
 */
public class Note extends Page {

	private String name; 		// ��� ��������
	private String userText; 	// �������� ������������� ����� � ���������

	private UserTextParser userTextParser; 	// �������� ������ �� ������ ������
	private StyleParser styleParser; 		// �������� ������ �� ������ html �����

	private String homeDirectoryPath; 		// ���� �� ����������, ���������� ������
											// ��������

	public Note(String urlOfNote) {

		super(urlOfNote);
		name = url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf(".")); // �������� ��� ������� �� url
		init(); // �������������� ��������� ��������

		// ��������� ������� ������ ��� ������ �������
		if (checkFilesExists()) { // ���� ����� ����
			userText = loadNoteFromFiles();
		} else {	// ���� ������ ���
			userText = "";
			createFiles();
		}
	}

	private void init() {
		userTextParser = Global.getInstance().getUserTextParser();
		styleParser = Global.getInstance().getStyleParser();

		homeDirectoryPath = Global.getInstance().getAppContext().getFilesDir().getAbsolutePath();
	}

	public String getUserText() {
		return userText;
	}

	public void setUserText(String value) {
		userText = value;
	}

	public String getName() {
		return name;
	}
	
	/*public void setName(String value) {
		name = value;
		url = "file://" + homeDirectoryPath + "/" + name + ".html";
	}*/

	/**
	 * ���������� html ��� ������ ��������
	 */
	public String getHtmlText() {
		return styleParser.addStyle(userTextParser.parse(userText));
	}

	/**
	 * ��������� �� ������ ����� � ����-��������� ��� ������ �������
	 * @return ����� � ����-���������
	 */
	public String loadNoteFromFiles() {
		try {
			FileInputStream inputstream = new FileInputStream(getPathToTextFile());
	
			InputStreamReader isr = new InputStreamReader(inputstream);
			BufferedReader reader = new BufferedReader(isr);
			String str;
			StringBuffer buffer = new StringBuffer();
	
			while ((str = reader.readLine()) != null) {
				buffer.append(str + "\n");
			}
	
			inputstream.close();
			return buffer.toString();
		} catch (Throwable t) {
			Toast.makeText(Global.getInstance().getAppContext(), "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
			return "";
		}
	}

	/**
	 * ��������� ������� � �����
	 */
	public void saveNoteToFile() {
		saveToFile(userText, getPathToTextFile());
		saveToFile(getHtmlText(), getPathToHtmlFile());
	}

	/**
	 * ��������� � ���� fileName ������ stringForSave
	 * @return true � ������ ��������� ����������. ����� false
	 */
	public boolean saveToFile(String stringForSave, String fileName) {
		try {
			FileOutputStream outputstream = new FileOutputStream(fileName);
			OutputStreamWriter osw = new OutputStreamWriter(outputstream);
			osw.write(stringForSave);
			osw.close();
			return true;
		} catch (Throwable t) {
			Toast.makeText(Global.getInstance().getAppContext(), "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
			return false;
		}
	}

	private String getPathToHtmlFile() {
		return homeDirectoryPath + "/" + name + ".html";
	}

	private String getPathToTextFile() {
		return homeDirectoryPath + "/" + name + ".txt";
	}

	/**
	 * ��������� ������� ������ ��� ������ �������
	 * 
	 * @return true, ���� ����� ��� ������� ����������. ����� false
	 */
	private boolean checkFilesExists() {
		File txtFile = new File(getPathToTextFile());
		return txtFile.exists();

	}

	/**
	 * ������ �����, ����������� ��� ������ �������
	 */
	private void createFiles() {
		File txtFile = new File(getPathToTextFile());
		File htmlFile = new File(getPathToHtmlFile());

		try {
			txtFile.createNewFile();
			htmlFile.createNewFile();
			FileWriter htmlWriter = new FileWriter(htmlFile);
			htmlWriter.write(getHtmlText());
			htmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
